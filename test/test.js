const { checkAge, checkFullName } = require('../src/util.js');
const { assert, expect } = require('chai');

describe('test_checkAge', () => {
    // check if it is NOT empty
    it('age_not_empty', () => {
        const age = '';
        assert.isNotEmpty(checkAge(age));
    });

    // check if it is a integer value
    it('age_is_integer', () => {
        const age = 12;
        // assert.isNumber(checkAge(age), "Error: age should be an integer");
        expect(checkAge(age)).to.be.a("number");
    });

    // check if it is NOT undefined
    it('age_not_undefined', () => {
        let age;
        assert.isDefined(checkAge(age));
    });

    // check if it is NOT null
    it('age_not_null', () => {
        const age = null;
        assert.isNotNull(checkAge(age));
    });

    // check if it is NOT equal to 0
    it('age_not_0', () => {
        const age = 0;
        assert.notEqual(checkAge(age), 0);
    });
});

describe('test_checkFullName', () => {
    // check if it is NOT empty
    it('fullName_not_empty', () => {
        const fullName = '';
        assert.isNotEmpty(checkFullName(fullName));
    });

    // check if it is NOT null
    it('fullName_not_null', () => {
        const fullName = null;
        assert.isNotNull(checkFullName(fullName));
    });

    // check if it is NOT undefined
    it('fullName_not_undefined', () => {
        let fullName;
        assert.isDefined(checkFullName(fullName));
    });

    // check if it is a string data type
    it('fullName_is_string', () => {
        const fullName = 23;
        assert.isString(checkFullName(fullName));
    });

    // check if the character is NOT equal to 0
    it('fullName_length_not_equal_to_zero', () => {
        const fullName = "";
        assert.isNotEmpty(checkFullName(fullName));
    });
});