function checkAge(age) {
    // handle the error if the value is empty
    if (age == "") return "Error: age should NOT be empty";

    // handle the error if the value is undefined
    if (age === undefined) return "Error: age should NOT be undefined";

    // handle the error if the value is null
    if (age === null) return "Error: age should NOT be null";

    // handle the error if the value is equal to 0
    if (age === 0) return "Error: age should NOT be equal to 0";

    // handle the error if the value is not integer
    if (typeof (age) !== "number") return "Error: age must a number";

    return age;
}

function checkFullName(fullName) {
    // handle the error if the value is empty
    if (fullName === "") return "Error: fullName should NOT be empty";

    // handle the error if the value is null
    if (fullName === null) return "Error: fullName should NOT be null";

    // handle the error if the value is undefined
    if (fullName === undefined) return "Error: fullName should NOT be undefined";

    // handle the error if the value is NOT string
    if (fullName !== "string") return "Error: fullName should be a string";

    // handle the error if the character length is equal to 0
    if (fullName === "") return "Error: character length should NOT be equal to 0";

    return fullName;
}

module.exports = {
    checkAge: checkAge,
    checkFullName: checkFullName
}